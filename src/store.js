import { createStore, applyMiddleware } from "redux";
import rootReducer from './reducers'
import thunk from "redux-thunk";

const configStore = (initialStore) => createStore(
  rootReducer,
  initialStore,
  applyMiddleware(thunk)
);

export default configStore({});
