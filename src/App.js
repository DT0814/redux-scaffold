import React, {Component} from 'react';
import './App.less';
import { Provider } from "react-redux";
import store from "./store";
import Name from "./Name";

class App extends Component{
  render() {
    return (
      <Provider store={store}>
        <div className='App'>
          <Name/>
        </div>
      </Provider>
    );
  }
}

export default App;
