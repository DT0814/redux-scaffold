const initState = {
  name: 'Friend'
};

export default (state = initState, action) => {
  switch (action.type) {
    case 'CHANGE_NAME':
      return {
        ...state,
        name: action.name
      };
    default :
      return state
  }
}
