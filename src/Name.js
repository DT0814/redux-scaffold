import React, { Fragment } from 'react'
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { setName } from "./actions/nameActions";
import nameReducers from "./reducers/nameReducers";

class Name extends React.Component {

  constructor(props) {
    super(props);
  }

  handlerChangeName(event) {
    this.props.setName(event.target.value);
  }

  render() {
    return (
      <Fragment>
        <input onChange={this.handlerChangeName.bind(this)} type='text'/>
        <p>
          Hello, {this.props.nameReducers.name}!
        </p>
      </Fragment>
    )
  }
}

const mapDispatchToProps = (dispatch) => bindActionCreators(
  { setName }
  , dispatch);

const mapStateToProps = (state) => ({
  nameReducers: state.nameReducers
});

export default connect(mapStateToProps, mapDispatchToProps)(Name)
